import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {JwtHelperService} from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {


  constructor(
    private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.isUserLoggedIn()) {
      const token: any = localStorage.getItem('user');
      const jwt: JwtHelperService = new JwtHelperService();

      const info = jwt.decodeToken(token);
      switch (info.role[0].name) {
        case "ROLE_ADMIN":
          this.router.navigate(['/admin-homepage']);
          break;
        case "ROLE_MANAGER":
          this.router.navigate(['/manager-homepage']);
          break;
      }
      return false;
    }
    return true;
  }

  private isUserLoggedIn(): boolean {
    if (!sessionStorage.getItem('token')) {
      return false;
    }
    return true;
  }

}
