import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { InteresovanjeServiceService } from 'src/app/service/interesovanje-service/interesovanje-service.service';
import { Interesovanje } from 'src/app/shared/interesovanje';

@Component({
  selector: 'app-interesovanje',
  templateUrl: './interesovanje.component.html',
  styleUrls: ['./interesovanje.component.sass']
})
export class InteresovanjeComponent implements OnInit {

  itemForm!: FormGroup;
  interesovanje!: Interesovanje; //interesovanje interfejs

  jesteDavalacKrviSelektovan: boolean = false;
  nijeDavalacKrviSelektovan: boolean = false;
  selektovanoDrzavljanstvo: string = "";

  //izabrani proizvodjaci vakcine za primanje..
  izabraniProizvodjaci: string[] = []

  izabranaMapaProizvodjacaVakcine: any = {};
 //item interfejs 
  //wine: Wine;
  //wineForm: FormGroup;
  //number: number;

  drzavljanstvo: String[] = []; //drzvaljasntva opcije
  proizvodjaciVakcine: String[] = [] //proizvodjaci vakcine koje su na raspolaganju da se prime..

  //category: string; //izabrana selektovana kategorija

  xmlInteresovanje: string = '';

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private interesovanjeService: InteresovanjeServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.createForm();
  }

  ngOnInit( ) {
    //this.number = this.route.snapshot.params.broj as number;
    this.drzavljanstvo = this.interesovanjeService.getDrzavljanstvo();
    this.proizvodjaciVakcine = this.interesovanjeService.loadProizvodjaciVakcina();
    //console.log(this.SubCategory);
    //console.log("Lista alergena je: " + this.alergen);

    //this.regularDugmicSelektovan = false;
    //this.luxDugmicSelektovan = false;

  }

  createForm() {
    this.itemForm = this.fb.group({
      jmbg: [
        "", [Validators.required, Validators.minLength(2)]
      ],
      drzavljanstvo: ["", Validators.required],
      ime: ["", Validators.required],
      prezime: ["", Validators.required],
      email: ["", Validators.required],
      lokacija: ["", Validators.required],
      brojMobilnog: ["", Validators.required],
      brojFixnog: ["", Validators.required],
      proizvodjaciVakcine: ["", Validators.required],
      davalacKrviDa: [""],
      davalacKrviNe: [""]
      
    });
  }

  onSubmit() {
    this.interesovanje = this.itemForm.value;
    this.interesovanje.drzavljanstvo = this.selektovanoDrzavljanstvo;
    
    //sad proizvodjace vakcine dodajemo
    this.interesovanje.proizvodjaciVakcine = this.izabraniProizvodjaci;

    //pravimo mapu proizvodjaca vakcine
    for (let p of this.interesovanjeService.loadProizvodjaciVakcina()){
        //let dodatiApostrofiNaNaziv: string = "'" + p + "'";
        this.izabranaMapaProizvodjacaVakcine[p] = false;
    }

    console.log("Ovo je kreirana mapa proizvodjaca: ");
    console.log(this.izabranaMapaProizvodjacaVakcine);

    //klucevi mape
    let kljuceviMape = Object.keys(this.izabranaMapaProizvodjacaVakcine);

    //ako je neka vakcina izarana stavi da je true
    for(let p of this.izabraniProizvodjaci){
      let nazivVakcine: any[]= p.split(" ");
      let uredjenNaziv = nazivVakcine[1].substr(1, nazivVakcine[1].length-2);
      this.izabranaMapaProizvodjacaVakcine[uredjenNaziv] = true;
    }
    console.log("nakon izmene: ");
    console.log(this.izabranaMapaProizvodjacaVakcine);

    

    //postavljamo informaciju da li je dobrovoljni davalac krvi
    if( this.jesteDavalacKrviSelektovan == true){
      this.interesovanje.davalacKrvi = true;
    }
    else{
      this.interesovanje.davalacKrvi = false;
    }

    console.log("Ispis poslatih podataka kroz formu: " + this.interesovanje.drzavljanstvo  + ", ime: " + this.interesovanje.ime + "prezime " + this.interesovanje.prezime+
    
    " jmbg" + this.interesovanje.jmbg + ", lokacija: " + this.interesovanje.lokacija + 
    " ,broj mobilnog: " + this.interesovanje.brojMobilnog + " , broj fixnog: " + this.interesovanje.brojFixnog + " ,da li je davalac krvi" + this.interesovanje.davalacKrvi
    + ", lista proizvodjaca vakcine: " + this.interesovanje.proizvodjaciVakcine);
    

    console.log(this.izabranaMapaProizvodjacaVakcine[kljuceviMape[0]]);
    console.log("VVVVVVVVVVVV: " + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[1]]);
    console.log(this.izabranaMapaProizvodjacaVakcine[kljuceviMape[2]]);
    console.log(this.izabranaMapaProizvodjacaVakcine[kljuceviMape[3]]);
    console.log(this.izabranaMapaProizvodjacaVakcine[kljuceviMape[4]]);
    console.log(this.izabranaMapaProizvodjacaVakcine[kljuceviMape[5]]);

    this.xmlInteresovanje = `<?xml version="1.0" encoding="UTF-8"?>
    <inter:interesovanje xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning"
                   xmlns:inter="www.ftn.uns.ac.rs/interesovanje"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xmlns:pred="http://www.ftn.uns.ac.rs/rdf/interesovanje/predicate/"
                   xsi:schemaLocation="www.ftn.uns.ac.rs/interesovanje file:/C:/Users/Bogdana/Desktop/zamenjeneSeme/prosireneRdfSeme/Interesovanje.xsd" datum="2021-12-12" id="document1">
    
        <inter:zainteresovana_osoba about="http://www.ftn.uns.ac.rs/rdf/interesovanje/zainteresovana_osoba">
            <inter:drzavljanstvo>` + this.interesovanje.drzavljanstvo + `</inter:drzavljanstvo>
            <inter:jmbg>` + this.interesovanje.jmbg  + `</inter:jmbg>
            <inter:ime property="pred:ime">` + this.interesovanje.ime + `</inter:ime>
            <inter:prezime property="pred:prezime">` + this.interesovanje.prezime + `</inter:prezime>
            <inter:aderesa_elektronske_poste property="pred:email">` + this.interesovanje.email + `</inter:aderesa_elektronske_poste>
            <inter:broj_mobilnog>` + this.interesovanje.brojMobilnog + `</inter:broj_mobilnog>
            <inter:broj_fixnog_telefona>` + this.interesovanje.brojFixnog + `</inter:broj_fixnog_telefona>
            <inter:dobrovoljni_davalac_krvi property="pred:dobrovoljni_davalac">` + this.interesovanje.davalacKrvi + `</inter:dobrovoljni_davalac_krvi>
        </inter:zainteresovana_osoba>
        <inter:lokacija_za_primanje_vakcine>` + this.interesovanje.lokacija + `</inter:lokacija_za_primanje_vakcine>
        <inter:odabir_proizvodjaca_vakcine>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Pfizer</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[0]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Sinopharm</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[1]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Astra Zenka</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[2]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Sputnik V</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[3]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Moderna</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[4]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Bilo koja</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[5]] + `</inter:izabran>
            </inter:proizvodjac>
        </inter:odabir_proizvodjaca_vakcine>
    </inter:interesovanje>`;
        

    this.interesovanjeService.add(this.xmlInteresovanje).subscribe((data: any) => {
      console.log("Ovo su podaci nakon dodavanja interesovanja...");
      console.log(data);
      alert("Interesovanje za osobu sa nazivom " + data.ime  + ", " + data.prezime + " je dodato...");
      
    });
  }


  //metoda koja se poziva prilikom izbora vrste komponente
  selectChangeHandlerDrzavljanstvo (event: any) {
    console.log("Promenjena vrednost drzavljanstava.." + event.target.value);
    this.selektovanoDrzavljanstvo = event.target.value;
    console.log("Selektovana vrednost je: " + this.selektovanoDrzavljanstvo);
  }

  //metoda kojom se selektuje i cuva izabrani proizvodjac vakcine..
  selectChangeHandlerProizvodjaciVakcine(event: any){
    console.log("Promenjena vrednost proizvodjaca vakcine.." + event.target.value);
    this.izabraniProizvodjaci.push(event.target.value);  //dodajemo u listu izabranih proizvodjaca
    console.log("Selektovane vrednosti proizvodjaca vakcine je: " + this.izabraniProizvodjaci);
  }

  //da li je davalac krvi --- dogadja radio button klik
  handleChangeRadioButton(event: any){
    console.log("Kliknuto je radio button...");

    let target = event.target;

    console.log("TArget je: "  + target);

    if(target.checked){
      console.log("Selektovacu ovo....");
      //trebamo onaj drugi dugmic da deselektujemo automatski
      if(target.value == "DA"){
        console.log("Selektovan je da jeste davalac krvi...");
        this.jesteDavalacKrviSelektovan = true;
        this.nijeDavalacKrviSelektovan = false;
        
        
      }
      else{
        console.log("Selektovan je da nije davalac krvi dugmic..");
        this.jesteDavalacKrviSelektovan = false;
        this.nijeDavalacKrviSelektovan = false;
        //sad ako je jedan selektovan drugi ce automatski biti deselektovan..
      }
    }
    console.log("Status jeste davalac krvi je: " + this.jesteDavalacKrviSelektovan + ", status nije davalac krvi je: " + this.nijeDavalacKrviSelektovan);
  }

}
