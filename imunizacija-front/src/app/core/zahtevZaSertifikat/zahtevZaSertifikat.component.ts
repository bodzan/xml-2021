import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ZahtevZaSertifikatServiceService } from 'src/app/service/zahtevZaSertifikat-service/zahtevZaSertifikat-service.service';
//import { InteresovanjeServiceService } from 'src/app/service/interesovanje-service/interesovanje-service.service';
import { ZahtevZaSertifikat } from 'src/app/shared/zahtevZaSertifikat';

declare const Xonomy: any
@Component({
  selector: 'app-zahtevZaSertifikat',
  templateUrl: './zahtevZaSertifikat.component.html',
  styleUrls: ['./zahtevZaSertifikat.component.sass']
})
export class ZahtevZaSertifikatComponent implements OnInit {

  formaPodaci!: FormGroup;
  zahtevZaSertifikat!: ZahtevZaSertifikat;

  //pol podnosioca zahteva
  polJeMuski: boolean = false;
  polJeZenski: boolean = false;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private ZahtevZaSertifikatService: ZahtevZaSertifikatServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.createForm();
  }


  ngOnInit(): void {
    console.log("pokretanje...");
  }

  ngAfterViewInit(): void{
    let elemRaz = document.getElementById("razlog");
    console.log("Naziv taga je "+elemRaz?.tagName);
    let specifikacijaRazloga = this.ZahtevZaSertifikatService.razlogSpecifikacija;
    let xmlString = "<Razlog_podnosenja_zahteva> </Razlog_podnosenja_zahteva>";
    Xonomy.render( xmlString, elemRaz, specifikacijaRazloga);
  }

  createForm() {
    this.formaPodaci = this.fb.group({
      jmbg: [
        "", [Validators.required, Validators.minLength(2)]
      ],
      datumRodjenja: ["", Validators.required],
      ime: ["", Validators.required],
      prezime: ["", Validators.required],
      brojPasosa: ["", Validators.required],
      razlogPodnosenjaZahteva: ["", Validators.required],
      pol: ["", Validators.required],
      polMuski: [],
      polZenski: []
      
    });
  }


  onSubmit() {
    this.zahtevZaSertifikat = this.formaPodaci.value;
    
  
    //postavaljamo informaciju o polu podnosioca zahteva
    if( this.polJeMuski == true){
      this.zahtevZaSertifikat.pol = "MUSKI";
    }
    else{
      this.zahtevZaSertifikat.pol = "ZENSKI";
    }

    //dobavljanje podataka o razlogu za slanje zahteva za sertifikat
    //dobavljanje podataka iz Xonomy rich text forme
    let xonomyRazlog = Xonomy.harvest();
    this.zahtevZaSertifikat.razlogPodnosenjaZahteva = xonomyRazlog;
    console.log("Xonomy podaci dobavljeni su: " + this.zahtevZaSertifikat.razlogPodnosenjaZahteva);

    console.log("Kliknuto je na na SUBMIT....");
    console.log("Ispis poslatih podataka kroz formu: " + this.zahtevZaSertifikat.datumRodjenja  + 
    ", ime: " + this.zahtevZaSertifikat.ime + "prezime " + this.zahtevZaSertifikat.prezime+
    
    " jmbg" + this.zahtevZaSertifikat.jmbg +
    ", broj pasosa: " + this.zahtevZaSertifikat.brojPasosa + 
    " ,razlog podnosenja zahteva: " + this.zahtevZaSertifikat.razlogPodnosenjaZahteva +
    " , pol podnosioca zahteva: " + this.zahtevZaSertifikat.pol);
    

    
    

    /**this.xmlInteresovanje = `<?xml version="1.0" encoding="UTF-8"?>
    <inter:interesovanje xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning"
                   xmlns:inter="www.ftn.uns.ac.rs/interesovanje"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xmlns:pred="http://www.ftn.uns.ac.rs/rdf/interesovanje/predicate/"
                   xsi:schemaLocation="www.ftn.uns.ac.rs/interesovanje file:/C:/Users/Bogdana/Desktop/zamenjeneSeme/prosireneRdfSeme/Interesovanje.xsd" datum="2021-12-12" id="document1">
    
        <inter:zainteresovana_osoba about="http://www.ftn.uns.ac.rs/rdf/interesovanje/zainteresovana_osoba">
            <inter:drzavljanstvo>` + this.interesovanje.drzavljanstvo + `</inter:drzavljanstvo>
            <inter:jmbg>` + this.interesovanje.jmbg  + `</inter:jmbg>
            <inter:ime property="pred:ime">` + this.interesovanje.ime + `</inter:ime>
            <inter:prezime property="pred:prezime">` + this.interesovanje.prezime + `</inter:prezime>
            <inter:aderesa_elektronske_poste property="pred:email">` + this.interesovanje.email + `</inter:aderesa_elektronske_poste>
            <inter:broj_mobilnog>` + this.interesovanje.brojMobilnog + `</inter:broj_mobilnog>
            <inter:broj_fixnog_telefona>` + this.interesovanje.brojFixnog + `</inter:broj_fixnog_telefona>
            <inter:dobrovoljni_davalac_krvi property="pred:dobrovoljni_davalac">` + this.interesovanje.davalacKrvi + `</inter:dobrovoljni_davalac_krvi>
        </inter:zainteresovana_osoba>
        <inter:lokacija_za_primanje_vakcine>` + this.interesovanje.lokacija + `</inter:lokacija_za_primanje_vakcine>
        <inter:odabir_proizvodjaca_vakcine>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Pfizer</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[0]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Sinopharm</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[1]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Astra Zenka</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[2]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Sputnik V</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[3]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Moderna</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[4]] + `</inter:izabran>
            </inter:proizvodjac>
            <inter:proizvodjac>
                <inter:naziv_proizvodjaca>Bilo koja</inter:naziv_proizvodjaca>
                <inter:izabran>` + this.izabranaMapaProizvodjacaVakcine[kljuceviMape[5]] + `</inter:izabran>
            </inter:proizvodjac>
        </inter:odabir_proizvodjaca_vakcine>
    </inter:interesovanje>`;
        

    this.interesovanjeService.add(this.xmlInteresovanje).subscribe((data: any) => {
      console.log("Ovo su podaci nakon dodavanja interesovanja...");
      console.log(data);
      alert("Interesovanje za osobu sa nazivom " + data.ime  + ", " + data.prezime + " je dodato...");
      
    });*/
  }

  //koji je pol podnosioca zahteva--- dogadja radio button klik
  handleChangeRadioButton(event: any){
    console.log("Kliknuto je radio button...");

    let target = event.target;

    console.log("TArget je: "  + target);

    if(target.checked){
      console.log("Selektovacu ovo....");
      //trebamo onaj drugi dugmic da deselektujemo automatski
      if(target.value == "ZENSKI"){
        console.log("Selektovan je da je pol zenski...");
        this.polJeZenski = true;
        this.polJeMuski = false;
        
        
      }
      else{
        console.log("Selektovan je da je pol muski ");
        this.polJeZenski = false;
        this.polJeMuski = true;
        //sad ako je jedan selektovan drugi ce automatski biti deselektovan..
      }
    }
    console.log("Status pol je mujski je: " + this.polJeMuski+ ", status pol je zenski je: " + this.polJeZenski);
  }
}