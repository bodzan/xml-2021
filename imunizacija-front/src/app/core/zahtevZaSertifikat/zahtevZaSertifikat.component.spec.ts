import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtevZaSertifikatComponent } from './zahtevZaSertifikat.component';

describe('InteresovanjeComponent', () => {
  let component: ZahtevZaSertifikatComponent;
  let fixture: ComponentFixture<ZahtevZaSertifikatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZahtevZaSertifikatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZahtevZaSertifikatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
