import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';


import { catchError, retry } from 'rxjs/operators';
import { ZahtevZaSertifikat } from 'src/app/shared/zahtevZaSertifikat';


@Injectable({
  providedIn: 'root'
})
export class ZahtevZaSertifikatServiceService {

  // Define API
  apiURL = 'http://localhost:8085/api/';
  private authHeder = new HttpHeaders();
  //private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient) {
    //this.authHeader.append('Content-Type', 'application/json');
  }

  //specifikacija za xonomy >> rich text za unos razloga za podnosenje zahteva
  public razlogSpecifikacija = {
    elements:{
      fakultet:{
        menu: [],
        attributes: {}
      }
    }
  }
 
  //************* kreiranje novog zahteva za digitalni sertifikat i slanje **********
  add(noviZahtevXml: string): Observable<ZahtevZaSertifikat> {
    return this.http.post<ZahtevZaSertifikat>(this.apiURL + "interesovanje/createInteresovanje", noviZahtevXml, {headers: this.authHeder, responseType: 'json'})
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  /** CreateEmptyOrder(tableId : number): Observable<Order> {
    return this.http.post<Order>(this.apiURL+'api/waiter/createEmptyNewOrder/'+tableId,null)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } */

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}



