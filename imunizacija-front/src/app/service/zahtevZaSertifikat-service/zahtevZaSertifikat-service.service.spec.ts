import { TestBed } from '@angular/core/testing';

import { ZahtevZaSertifikatServiceService } from './zahtevZaSertifikat-service.service';

describe('ZahtevZaSertifikatServiceService', () => {
  let service: ZahtevZaSertifikatServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ZahtevZaSertifikatServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
