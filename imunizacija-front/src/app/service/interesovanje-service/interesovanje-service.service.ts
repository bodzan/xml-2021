import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Interesovanje } from 'src/app/shared/interesovanje';


import { catchError, retry } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InteresovanjeServiceService {

  // Define API
  apiURL = 'http://localhost:8085/api/';
  private authHeder = new HttpHeaders();
  //private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient) {
    //this.authHeader.append('Content-Type', 'application/json');
  }

  //metoda vraca listu svih kategorija stavki za jela
  getDrzavljanstvo(): string[]{
    return ['SRPSKO', 'STRANO_SA_BORAVKOM_U_SRBIJI', 'STRANO_BEZ_BORAVKA_U_SRBIJI'];
  }

  //metoda vraca sve proizvodjace vakcina
  loadProizvodjaciVakcina(): string[]{
    return ['Pfizer', "Sinopharm", 'Astra-Zenka', "Sputnik-V", "Moderna", "Bilo-koja" ]
  }

  //************* kreiranje novog interesovanja i slanje **********
  add(novoInteresovanjeXml: string): Observable<Interesovanje> {
    return this.http.post<Interesovanje>(this.apiURL + "interesovanje/createInteresovanje", novoInteresovanjeXml, {headers: this.authHeder, responseType: 'json'})
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  /** CreateEmptyOrder(tableId : number): Observable<Order> {
    return this.http.post<Order>(this.apiURL+'api/waiter/createEmptyNewOrder/'+tableId,null)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } */

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}



