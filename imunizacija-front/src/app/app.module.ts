import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {AuthInterceptor} from "./interceptor/auth.interceptor";
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from "./material/material.module";
import { MainComponent } from './core/main/main.component';
import {ToastrModule} from "ngx-toastr";
import {ReactiveFormsModule} from "@angular/forms";
import { LoginComponent } from './core/login/login.component';

import { RegisterComponent } from './core/register/register.component';
import { SaglasnostComponent } from './core/saglasnost/saglasnost.component';

import { InteresovanjeComponent } from './core/interesovanje/interesovanje.component';
import { ZahtevZaSertifikatComponent } from './core/zahtevZaSertifikat/zahtevZaSertifikat.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    RegisterComponent,
    SaglasnostComponent,
    InteresovanjeComponent,
    ZahtevZaSertifikatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
