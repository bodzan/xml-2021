import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./core/login/login.component";
import {RegisterComponent} from "./core/register/register.component";

import { InteresovanjeComponent } from './core/interesovanje/interesovanje.component';
import { ZahtevZaSertifikatComponent } from './core/zahtevZaSertifikat/zahtevZaSertifikat.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'interesovanje', component: InteresovanjeComponent},
  {path: 'zahtevZaSertifikat', component: ZahtevZaSertifikatComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
