export class Interesovanje {
    jmbg: string;
    drzavljanstvo: string;
    ime: string;
    prezime: string;
    email: string;
    lokacija: string;
    brojMobilnog: number;
    brojFixnog: number;
    proizvodjaciVakcine: string[];
    davalacKrvi: boolean;

    constructor(){
        this.jmbg = "";
        this.drzavljanstvo = "";
        this.ime = "";
        this.prezime = "";
        this.email = "";
        this.lokacija = "";
        this.brojMobilnog =0;
        this.brojFixnog = 0;
        this.proizvodjaciVakcine = [];
        this.davalacKrvi = false;
    }
}
