export class ZahtevZaSertifikat {
    datumRodjenja: string;
    ime: string;
    prezime: string;
    jmbg: string;
    brojPasosa: string;
    pol: string;
    razlogPodnosenjaZahteva: string;
    

    constructor(){
        this.datumRodjenja = "";
        this.ime = "";
        this.prezime = "";
        this.jmbg = "";
        this.brojPasosa = "";
        this.pol = "";
        this.razlogPodnosenjaZahteva = "";
    }
}
