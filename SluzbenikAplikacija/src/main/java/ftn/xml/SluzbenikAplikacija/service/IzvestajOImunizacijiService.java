package ftn.xml.SluzbenikAplikacija.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.xml.SluzbenikAplikacija.repository.IzvestajOImunizacijiRepository;

@Service
public class IzvestajOImunizacijiService {
	
	@Autowired
	IzvestajOImunizacijiRepository izvestajRepository;
	
	public Object readIzvestaj(String documentId) {

        return izvestajRepository.findIzvestaj(documentId);
        
    }
	
	public void writeIzvestajXML(String documentId, String xml) throws Exception {
    	izvestajRepository.saveIzvestajXML(documentId, xml);
    }

}
