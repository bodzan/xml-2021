package ftn.xml.SluzbenikAplikacija.service;

import ftn.xml.SluzbenikAplikacija.model.rs.ac.uns.ftn.korisnici.Korisnici;
import ftn.xml.SluzbenikAplikacija.model.rs.ac.uns.ftn.korisnik.Korisnik;
import ftn.xml.SluzbenikAplikacija.repository.KorisnikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


@Service
public class KorisnikService {

    @Autowired
    private KorisnikRepository korisnikRepository;

//    @Autowired
//    private KorisnikRDFRepository repoRdf;

    public boolean saveUser(Korisnik korisnik) {
        return this.korisnikRepository.saveUser(korisnik);
    }


//    public void makeRDF() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, XMLDBException, JAXBException {
//        Korisnici korisnici = this.repo.getAllKorisnici();
//        this.repoRdf.saveRdf(korisnici);;
//
//    }


    public Korisnik login(String username) {
        return this.korisnikRepository.login(username);
    }


    public Korisnik getLoggedUser() throws Exception {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) auth
                    .getPrincipal();
            return korisnikRepository.login(user.getUsername());
        } catch (Exception e) {
            throw new Exception("Nije pronadjen ulogovani korisnik!");
        }

    }

    public Korisnik getUser(String id) {
        return korisnikRepository.getUserById(id);
    }

    public Korisnici getAllUsers() {
        return korisnikRepository.getAllKorisnici();
    }

}
