package ftn.xml.SluzbenikAplikacija;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SluzbenikAplikacijaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SluzbenikAplikacijaApplication.class, args);
	}

}
