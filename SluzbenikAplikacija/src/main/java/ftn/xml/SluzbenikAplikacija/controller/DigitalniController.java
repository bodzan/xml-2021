package ftn.xml.SluzbenikAplikacija.controller;

import ftn.xml.SluzbenikAplikacija.service.DigitalniSertifikatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/digitalni")
public class DigitalniController {

    @Autowired
    public DigitalniSertifikatService digitalniSertifikatService;


    @GetMapping(value = "/read/{id}", produces = "application/xml")
    public ResponseEntity<Object> findDigitalni(@PathVariable String id) {

        try {
            Object digitalni = digitalniSertifikatService.findFindDigitalniById(id);
            return new ResponseEntity<>(digitalni, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/create")
    public ResponseEntity<String> writeDigitalniXml(@RequestBody String xml) {
        try {


            digitalniSertifikatService.saveDigitalniXML(xml);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }

    }
}
