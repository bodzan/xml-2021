package ftn.xml.SluzbenikAplikacija.controller;

import ftn.xml.SluzbenikAplikacija.model.rs.ac.uns.ftn.korisnici.Korisnici;
import ftn.xml.SluzbenikAplikacija.model.rs.ac.uns.ftn.korisnik.Korisnik;
import ftn.xml.SluzbenikAplikacija.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {

    @Autowired
    public KorisnikService korisnikService;


    @RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getUserById(@PathVariable String id) {

        try {
            Korisnik k = korisnikService.getUser(id);
            return new ResponseEntity<>(k.getIdKorisnika(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
    public ResponseEntity<Korisnici> getAllUsers() {
        Korisnici k = korisnikService.getAllUsers();

        return new ResponseEntity<>(k, HttpStatus.OK);
    }
}
