package ftn.xml.SluzbenikAplikacija.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ftn.xml.SluzbenikAplikacija.service.IzvestajOImunizacijiService;

@RestController
@RequestMapping(value = "/api/izvestajOImunizaciji")
public class IzvestajOImunizacijiController {
	
	@Autowired
	IzvestajOImunizacijiService izvestajService;
	
	@RequestMapping(value = "/read", method = RequestMethod.GET, produces = "application/xml")
    public ResponseEntity<Object> readXmlFile(@RequestParam String documentId) {
        try {
            Object izvestaj = izvestajService.readIzvestaj(documentId);

            return new ResponseEntity<>(izvestaj, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
	
	@RequestMapping(value = "/create/{documentId}", method = RequestMethod.POST, consumes = "application/xml")
    public ResponseEntity<String> writeIzvestajOImunizacijiXML(@PathVariable String documentId, @RequestBody String xml) {
        try {
        	izvestajService.writeIzvestajXML(documentId, xml);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
