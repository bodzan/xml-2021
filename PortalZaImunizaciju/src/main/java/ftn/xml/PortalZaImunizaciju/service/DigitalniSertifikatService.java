package ftn.xml.PortalZaImunizaciju.service;

import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.digitalni_sertifikat.DigitalCertificate;
import ftn.xml.PortalZaImunizaciju.repository.DigitalniRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

@Service
public class DigitalniSertifikatService {

    @Autowired
    public DigitalniRepository digitalniRepository;


    public void saveDigitalniXML(String xml) throws Exception {

        File file = new File("src/main/resources/xmlFiles/xhtml/digitalni_sertifikat.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(DigitalCertificate.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../xml-documents/seme/digitalni_sertifikat.xsd"));
        unmarshaller.setSchema(schema);

        DigitalCertificate saglasnost = (DigitalCertificate) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/xhtml/digitalni_sertifikat.xml"));

        if(saglasnost.getCertificateData().getCertificateId().equals(""))
            saglasnost.getCertificateData().setCertificateId(UUID.randomUUID().toString());

        digitalniRepository.saveDigitalniXml(saglasnost);
        digitalniRepository.saveDigitalniXml(saglasnost);
    }


    public Object findFindDigitalniById(String id) {
        return digitalniRepository.findDigitalniXml(id);
    }
}
