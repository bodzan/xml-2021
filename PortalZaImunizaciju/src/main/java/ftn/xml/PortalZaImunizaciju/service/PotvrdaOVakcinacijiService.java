package ftn.xml.PortalZaImunizaciju.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.xml.PortalZaImunizaciju.model.potvrdaOVakcinaciji.PotvrdaOVakcinaciji;
import ftn.xml.PortalZaImunizaciju.repository.PotvrdaOVakcinacijiRepository;


import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

@Service
public class PotvrdaOVakcinacijiService {
	
	@Autowired
	PotvrdaOVakcinacijiRepository potvrdaRepository;
	
	public Object readPotvrda(String documentId) {

        return potvrdaRepository.findPotvrda(documentId);
    }

    public boolean writePotvrda(String fileName) {

        return potvrdaRepository.savePotvrda(fileName);
    }

    // OLD method for saving potvrda
    // public void writePotvrdaXml(String documentId, String xml) throws Exception {
    // 	potvrdaRepository.savePotvrdaXML(documentId, xml);
    // }

    public void writePotvrdaXml(String xml) throws Exception {
    	File file = new File("src/main/resources/xmlFiles/xhtml/potvrdaOVakcinaciji.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(PotvrdaOVakcinaciji.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../xml-documents/seme/PotvrdaOVakcinaciji.xsd"));
        unmarshaller.setSchema(schema);

        PotvrdaOVakcinaciji potvrda = (PotvrdaOVakcinaciji) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/xhtml/potvrdaOVakcinaciji.xml"));

        if(potvrda.getPotvrdaPodaci().getSifraPotvrde().equals(""))
            potvrda.getPotvrdaPodaci().setSifraPotvrde(UUID.randomUUID().toString());

        potvrdaRepository.savePotvrdaXML(potvrda);
        potvrdaRepository.savePotvrdaXML(potvrda);
    }

}
