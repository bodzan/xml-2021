package ftn.xml.PortalZaImunizaciju.service;

import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.saglasnost.SaglasnostZaSprovodjenjeImunizacije;
import ftn.xml.PortalZaImunizaciju.repository.SaglasnostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.UUID;

@Service
public class SaglasnostService {

    @Autowired
    public SaglasnostRepository saglasnostRepository;


    public void saveSaglasnostXML(String xml) throws Exception {

        File file = new File("src/main/resources/xmlFiles/xhtml/saglasnost.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(SaglasnostZaSprovodjenjeImunizacije.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../xml-documents/seme/saglasnost.xsd"));
        unmarshaller.setSchema(schema);

        SaglasnostZaSprovodjenjeImunizacije saglasnost = (SaglasnostZaSprovodjenjeImunizacije) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/xhtml/saglasnost.xml"));
        saglasnost.setId(UUID.randomUUID().toString());
        saglasnostRepository.saveSaglasnostXml(saglasnost);
//        saglasnostRepository.saveSaglasnostXml(saglasnost);
    }


    public Object findSaglasnostById(String id) {
        return saglasnostRepository.findSaglasnostXml(id);
    }
}
