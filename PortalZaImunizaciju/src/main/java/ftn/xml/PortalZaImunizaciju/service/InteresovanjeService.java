package ftn.xml.PortalZaImunizaciju.service;

import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.interesovanje.Interesovanje;
import ftn.xml.PortalZaImunizaciju.repository.InteresovanjeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

@Service
public class InteresovanjeService {

    @Autowired
    public InteresovanjeRepository interesovanjeRepository;

    public void saveInteresovanjeXml(String xml) throws Exception {

        //sad od prosledjenog stringa treba da napravimo xml fajl i da ga sacuvamo.

        File file = new File("src/main/resources/xmlFiles/xhtml/interesovanje.xml");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = xml.getBytes();
        //write byte array to file
        bos.write(bytes);
        bos.close();
        fos.close();

        JAXBContext context = JAXBContext.newInstance(Interesovanje.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("../xml-documents/seme/Interesovanje.xsd"));
        unmarshaller.setSchema(schema);

        Interesovanje interesovanje = (Interesovanje) unmarshaller.unmarshal(new File("src/main/resources/xmlFiles/xhtml/interesovanje.xml"));
        interesovanje.setId(UUID.randomUUID().toString());
        interesovanjeRepository.saveInteresovanjeXml(interesovanje);

    }


    public Object findSaglasnostById(String id) {
        return interesovanjeRepository.findInteresovanjeXml(id);
    }

}
