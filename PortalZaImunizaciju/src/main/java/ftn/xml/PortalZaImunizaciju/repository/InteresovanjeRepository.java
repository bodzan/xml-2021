package ftn.xml.PortalZaImunizaciju.repository;

import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.interesovanja.Interesovanja;
import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.interesovanje.Interesovanje;
import ftn.xml.PortalZaImunizaciju.util.MetadataExtractor;
import ftn.xml.PortalZaImunizaciju.util.RdfDbConnectionUtils;
import ftn.xml.PortalZaImunizaciju.util.XmlDbConnectionUtils;
import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class InteresovanjeRepository {
    public final String INTERESOVANJE_COLLECTION_NAME = "/db/interesovanje";
    public final String INTERESOVANJE_NAMED_GRAPH_URI = "/interesovanje/metadata";


    public Object findInteresovanjeXml(String id) {
        String collectionId = INTERESOVANJE_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(Interesovanja.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/interesovanje.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            Interesovanje saglasnost = (Interesovanje) unmarshaller.unmarshal(res.getContentAsDOM());
//
            JAXBContext saglasonstContext = JAXBContext.newInstance(Interesovanje.class);

            // create an instance of `Unmarshaller`
            Marshaller marshaller = saglasonstContext.createMarshaller();

            SchemaFactory saglasnostsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema saglasnostschema = saglasnostsf.newSchema(new File("../xml-documents/seme/interesovanje.xsd"));
            marshaller.setSchema(saglasnostschema);

            marshaller.marshal(saglasnost, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);
            xml = xmlResource.getContent();
            System.out.println("********* Procitano je: ");
            System.out.println(xml.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }


    public void saveInteresovanjeXml(Interesovanje interesovanje) throws Exception {
        String collectionId = INTERESOVANJE_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

            extractAndSaveMetadata();
            System.out.println("********** PROCITANI METAPODACI *******************");
            //readSaglasnostMetadata();
            System.out.println("***************************************************");

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);


            res = (XMLResource) col.createResource(interesovanje.getId(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(Interesovanje.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(interesovanje, os);

            res.setContent(os);

            col.storeResource(res);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }


    private void extractAndSaveMetadata() throws Exception {
        MetadataExtractor extractor = new MetadataExtractor();
        String xml = Files.readString(Path.of("src/main/resources/xmlFiles/xhtml/interesovanje.xml"));
        System.out.println("Ovo je string podataka: ");
        System.out.println(xml);
        InputStream in = new ByteArrayInputStream(xml.getBytes());

        OutputStream out = new FileOutputStream("src/main/resources/xmlFiles/rdf/metadata.rdf");

        extractor.extractMetadata(in, out);

        RdfDbConnectionUtils.writeMetadataToDatabase(INTERESOVANJE_NAMED_GRAPH_URI);
    }

    private void readSaglasnostMetadata() throws Exception {
        RdfDbConnectionUtils.loadMetadataFromDatabase(INTERESOVANJE_NAMED_GRAPH_URI);
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }
}
