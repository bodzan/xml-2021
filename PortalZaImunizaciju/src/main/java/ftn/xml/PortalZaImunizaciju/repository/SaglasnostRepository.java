package ftn.xml.PortalZaImunizaciju.repository;

import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.saglasnost.SaglasnostZaSprovodjenjeImunizacije;
import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.saglasnosti.Saglasnosti;
import ftn.xml.PortalZaImunizaciju.util.MetadataExtractor;
import ftn.xml.PortalZaImunizaciju.util.RdfDbConnectionUtils;
import ftn.xml.PortalZaImunizaciju.util.XmlDbConnectionUtils;
import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SaglasnostRepository {

    public final String SAGLASNOST_COLLECTION_NAME = "/db/saglasnost";
    public final String SAGLASNOST_NAMED_GRAPH_URI = "/saglasnost/metadata";


    public Object findSaglasnostXml(String id) {
        String collectionId = SAGLASNOST_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(Saglasnosti.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/saglasnost.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            SaglasnostZaSprovodjenjeImunizacije saglasnost = (SaglasnostZaSprovodjenjeImunizacije) unmarshaller.unmarshal(res.getContentAsDOM());
//
            JAXBContext saglasonstContext = JAXBContext.newInstance(SaglasnostZaSprovodjenjeImunizacije.class);

            // create an instance of `Unmarshaller`
            Marshaller marshaller = saglasonstContext.createMarshaller();

            SchemaFactory saglasnostsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema saglasnostschema = saglasnostsf.newSchema(new File("../xml-documents/seme/saglasnost.xsd"));
            marshaller.setSchema(saglasnostschema);

            marshaller.marshal(saglasnost, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);
            xml = xmlResource.getContent();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }

    public Object findSaglasnostClass(String id) {
        String collectionId = SAGLASNOST_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        SaglasnostZaSprovodjenjeImunizacije saglasnost = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(Saglasnosti.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/saglasnost.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            saglasnost = (SaglasnostZaSprovodjenjeImunizacije) unmarshaller.unmarshal(res.getContentAsDOM());



        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return saglasnost;
    }


    public void saveSaglasnostXml(SaglasnostZaSprovodjenjeImunizacije saglasnost) throws Exception {
        String collectionId = SAGLASNOST_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

//            extractAndSaveMetadata();
//            readSaglasnostMetadata();

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);


//            res = (XMLResource) col.getResource("saglasnosti.xml");

//            if (res == null) {
//                res = (XMLResource) col.createResource("saglasnosti.xml", XMLResource.RESOURCE_TYPE);
//                JAXBContext jaxbContext = JAXBContext.newInstance(Saglasnosti.class);
//                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//
//                Saglasnosti zahtevi = new Saglasnosti();
//                List<SaglasnostZaSprovodjenjeImunizacije> lista = new ArrayList<>();
//
//                zahtevi.setSaglasnostZaSprovodjenjeImunizacije(lista);
//                jaxbMarshaller.marshal(zahtevi, os);
//
//                res.setContent(os);
//
//                col.storeResource(res);
//            }

            res = (XMLResource) col.createResource(saglasnost.getId(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(SaglasnostZaSprovodjenjeImunizacije.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(saglasnost, os);

            res.setContent(os);

            col.storeResource(res);
//            os = new ByteArrayOutputStream();
//
//            // zahteve iz baze u objekat pa se doda u njega
//            JAXBContext contextZahtevi = JAXBContext.newInstance(Saglasnosti.class);
//            Unmarshaller unmarshaller = contextZahtevi.createUnmarshaller();
//
//            SchemaFactory sfZahtevi = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//            Schema schemaZahtevi = sfZahtevi.newSchema(new File("../xml-documents/seme/saglasnosti.xsd"));
//            unmarshaller.setSchema(schemaZahtevi);
//
//            Saglasnosti saglasnosti = (Saglasnosti) unmarshaller.unmarshal(res.getContentAsDOM());
//            JAXBContext context = JAXBContext.newInstance(ZahtevZaPristupInformacijama.class);
//            saglasnosti.getSaglasnostZaSprovodjenjeImunizacije().add(saglasnost);
//
//            Marshaller marshaller = contextZahtevi.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//            marshaller.setSchema(schemaZahtevi);
//            marshaller.marshal(saglasnosti, os);
//
//            res.setContent(os);
//
//            col.storeResource(res);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }


    private void extractAndSaveMetadata() throws Exception {
        MetadataExtractor extractor = new MetadataExtractor();
        String xml = Files.readString(Path.of("src/main/resources/xmlFiles/xhtml/saglasnost.xml"));
        InputStream in = new ByteArrayInputStream(xml.getBytes());

        OutputStream out = new FileOutputStream("src/main/resources/xmlFiles/rdf/metadata.rdf");

        extractor.extractMetadata(in, out);

        RdfDbConnectionUtils.writeMetadataToDatabase(SAGLASNOST_NAMED_GRAPH_URI);
    }

    private void readSaglasnostMetadata() throws Exception {
        RdfDbConnectionUtils.loadMetadataFromDatabase(SAGLASNOST_NAMED_GRAPH_URI);
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }

}
