package ftn.xml.PortalZaImunizaciju.repository;

import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import ftn.xml.PortalZaImunizaciju.model.potvrdaOVakcinaciji.PotvrdaOVakcinaciji;
import ftn.xml.PortalZaImunizaciju.util.JAXBReader;
import ftn.xml.PortalZaImunizaciju.util.MetadataExtractor;
import ftn.xml.PortalZaImunizaciju.util.RdfDbConnectionUtils;
import ftn.xml.PortalZaImunizaciju.util.XmlDbConnectionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

@Repository
public class PotvrdaOVakcinacijiRepository {
	
	private final String POTVRDA_COLLECTION_NAME = "/db/potvrdaOVakcinaciji";
    public final String POTVRDA_NAMED_GRAPH_URI = "/potvrda/metadata";
	
	public boolean savePotvrda(String documentId) {
        String collectionId = POTVRDA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);

            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + documentId);
            res = (XMLResource) col.createResource(documentId, XMLResource.RESOURCE_TYPE);

            // create an instance of `JAXBContext`
            JAXBContext context = JAXBContext.newInstance(PotvrdaOVakcinaciji.class);

            // create an instance of `Marshaller`
            Marshaller marshaller = context.createMarshaller();

            // enable pretty-print XML output
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/PotvrdaOVakcinaciji.xsd"));
            marshaller.setSchema(schema);

            PotvrdaOVakcinaciji potvrda = JAXBReader.writePotvrdaXML(documentId);
            // marshal the contents to an output stream
            marshaller.marshal(potvrda, os);

            // link the stream to the XML resource
            res.setContent(os);
            System.out.println("[INFO] Storing the document: " + res.getId());

            col.storeResource(res);
            System.out.println("[INFO] Done.");

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return true;
    }
	
    // OLD method for saving XML potvrda
	public void savePotvrdaXMLOLD(String documentId, String xml) throws Exception {
        String collectionId = POTVRDA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {

        	//TODO
            //extractAndSaveMetadata(documentId, xml);
            //readPotvrdaMetadata();

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);

            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + documentId);
            res = (XMLResource) col.createResource(documentId, XMLResource.RESOURCE_TYPE);

            // create an instance of `JAXBContext`
            JAXBContext context = JAXBContext.newInstance(PotvrdaOVakcinaciji.class);

            // create an instance of `Marshaller`
            Marshaller marshaller = context.createMarshaller();

            // enable pretty-print XML output
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/PotvrdaOVakcinaciji.xsd"));
            marshaller.setSchema(schema);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();
            unmarshaller.setSchema(schema);

            // convert XML file to object
            StringReader reader = new StringReader(xml);
            PotvrdaOVakcinaciji potvrda = (PotvrdaOVakcinaciji) unmarshaller.unmarshal(reader);
            // marshal the contents to an output stream
            marshaller.marshal(potvrda, os);

            // link the stream to the XML resource
            res.setContent(os);
            System.out.println("[INFO] Storing the document: " + res.getId());

            col.storeResource(res);
            System.out.println("[INFO] Done.");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }
	
	public Object findPotvrda(String documentId) {
        String collectionId = POTVRDA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;

        Object test = null;
        PotvrdaOVakcinaciji potvrda = null;
        OutputStream os = new ByteArrayOutputStream();
        try {
            // get the collection
            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            System.out.println("[INFO] Retrieving the document: " + documentId);
            res = (XMLResource)col.getResource(documentId);

            if(res == null) {
                System.out.println("[WARNING] Document '" + documentId + "' can not be found!");
            } else {
                JAXBContext context = JAXBContext.newInstance(PotvrdaOVakcinaciji.class);

                // create an instance of `Unmarshaller`
                Unmarshaller unmarshaller = context.createUnmarshaller();

                SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                Schema schema = sf.newSchema(new File("../xml-documents/seme/PotvrdaOVakcinaciji.xsd"));
                unmarshaller.setSchema(schema);

                // convert XML file to object
                potvrda = (PotvrdaOVakcinaciji) unmarshaller.unmarshal(res.getContentAsDOM());
                
                JAXBContext potvrdaContext = JAXBContext.newInstance(PotvrdaOVakcinaciji.class);

                // create an instance of `marshaller`
                Marshaller marshaller = potvrdaContext.createMarshaller();

                SchemaFactory saglasnostsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                Schema saglasnostschema = saglasnostsf.newSchema(new File("../xml-documents/seme/PotvrdaOVakcinaciji.xsd"));
                marshaller.setSchema(saglasnostschema);

                marshaller.marshal(potvrda, os);
                XMLResource xmlResource = (XMLResource) res;
                xmlResource.setContent(os);
                test = xmlResource.getContent();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return test;
    }


    public void savePotvrdaXML(PotvrdaOVakcinaciji potvrda) throws Exception {
        String collectionId = POTVRDA_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {
            //extractAndSaveMetadata();
//            readPotvrdaMetadata();

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);

            // getID
            res = (XMLResource) col.createResource(potvrda.getPotvrdaPodaci().getSifraPotvrde(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(PotvrdaOVakcinaciji.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            SchemaFactory sfZahtevi = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schemaZahtevi = sfZahtevi.newSchema(new File("../xml-documents/seme/digitalni_sertifikat.xsd"));

            jaxbMarshaller.setSchema(schemaZahtevi);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(potvrda, os);

            res.setContent(os);

            col.storeResource(res);
//            os = new ByteArrayOutputStream();
//
//            // zahteve iz baze u objekat pa se doda u njega
//            JAXBContext contextZahtevi = JAXBContext.newInstance(Saglasnosti.class);
//            Unmarshaller unmarshaller = contextZahtevi.createUnmarshaller();
//
//            SchemaFactory sfZahtevi = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//            Schema schemaZahtevi = sfZahtevi.newSchema(new File("../xml-documents/seme/saglasnosti.xsd"));
//            unmarshaller.setSchema(schemaZahtevi);
//
//            Saglasnosti saglasnosti = (Saglasnosti) unmarshaller.unmarshal(res.getContentAsDOM());
//            JAXBContext context = JAXBContext.newInstance(ZahtevZaPristupInformacijama.class);
//            saglasnosti.getSaglasnostZaSprovodjenjeImunizacije().add(saglasnost);
//
//            Marshaller marshaller = contextZahtevi.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//            marshaller.setSchema(schemaZahtevi);
//            marshaller.marshal(saglasnosti, os);
//
//            res.setContent(os);
//
//            col.storeResource(res);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }

    }


    private void extractAndSaveMetadata() throws Exception {
        MetadataExtractor extractor = new MetadataExtractor();
        String xml = Files.readString(Path.of("src/main/resources/xmlFiles/xhtml/PotvrdaOVakcinaciji1.xml"));
        InputStream in = new ByteArrayInputStream(xml.getBytes());

        OutputStream out = new FileOutputStream("src/main/resources/xmlFiles/rdf/metadata.rdf");

        extractor.extractMetadata(in, out);

        RdfDbConnectionUtils.writeMetadataToDatabase(POTVRDA_NAMED_GRAPH_URI);
    }

    private void readPotvrdaMetadata() throws Exception {
        RdfDbConnectionUtils.loadMetadataFromDatabase(POTVRDA_NAMED_GRAPH_URI);
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }

}
