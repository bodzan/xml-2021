package ftn.xml.PortalZaImunizaciju.repository;

import ftn.xml.PortalZaImunizaciju.model.potvrdaOVakcinaciji.PotvrdaOVakcinaciji;
import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.digitalni_sertifikat.DigitalCertificate;
import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.korisnici.Korisnici;
import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.potvrdE.PotvrdE;
import ftn.xml.PortalZaImunizaciju.util.MetadataExtractor;
import ftn.xml.PortalZaImunizaciju.util.RdfDbConnectionUtils;
import ftn.xml.PortalZaImunizaciju.util.XmlDbConnectionUtils;
import org.exist.xmldb.EXistResource;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.*;
import org.xmldb.api.modules.XMLResource;
import org.xmldb.api.modules.XQueryService;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Repository
public class DigitalniRepository {

    public final String DIGITALNI_COLLECTION_NAME = "/db/digitalni_sertifikat";
    public final String DIGITALNI_NAMED_GRAPH_URI = "/digitalni_sertifikat/metadata";


    public Object findDigitalniXml(String id) {
        String collectionId = DIGITALNI_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        Object xml = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(DigitalCertificate.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/digitalni_sertifikat.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            DigitalCertificate saglasnost = (DigitalCertificate) unmarshaller.unmarshal(res.getContentAsDOM());
//
            JAXBContext saglasonstContext = JAXBContext.newInstance(DigitalCertificate.class);

            // create an instance of `Unmarshaller`
            Marshaller marshaller = saglasonstContext.createMarshaller();

            SchemaFactory saglasnostsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema saglasnostschema = saglasnostsf.newSchema(new File("../xml-documents/seme/digitalni_sertifikat.xsd"));
            marshaller.setSchema(saglasnostschema);

            marshaller.marshal(saglasnost, os);
            XMLResource xmlResource = (XMLResource) res;
            xmlResource.setContent(os);
            xml = xmlResource.getContent();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return xml;
    }

    public Object findDigitalniClass(String id) {
        String collectionId = DIGITALNI_NAMED_GRAPH_URI;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();
        DigitalCertificate saglasnost = null;

        try {
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);
            col.setProperty(OutputKeys.INDENT, "yes");

            res = (XMLResource)col.getResource(id);

            JAXBContext context = JAXBContext.newInstance(DigitalCertificate.class);

            // create an instance of `Unmarshaller`
            Unmarshaller unmarshaller = context.createUnmarshaller();

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("../xml-documents/seme/digitalni_sertifikat.xsd"));
            unmarshaller.setSchema(schema);

            // convert XML file to object
            saglasnost = (DigitalCertificate) unmarshaller.unmarshal(res.getContentAsDOM());



        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //don't forget to clean up!

            if(res != null) {
                try {
                    ((EXistResource)res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if(col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        return saglasnost;
    }


    public void createDigitalniByJMBG(String JMBG){
        PotvrdE potvrde = getVaccinesFromPotvrda(JMBG);
        for (ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.potvrdE.PotvrdaOVakcinaciji p:potvrde.getPotvrdaOVakcinaciji()) {
            System.out.println(p.getPotvrdaPodaci().toString());
        }
    }

    public PotvrdE getVaccinesFromPotvrda(String JMBG){
        System.out.println("[INFO] " + DigitalniRepository.class.getSimpleName());

        // initialize collection and document identifiers
        String collectionId = null;
        String xqueryFilePath = null, xqueryExpression = null;

        System.out.println("[INFO] Using defaults.");
        collectionId = "/db/potvrdaOVakcinaciji";
        xqueryFilePath = "src\\main\\resources\\query\\getPotvrdEByJMBG.xqy";

        System.out.println("\t- collection ID: " + collectionId);
        System.out.println("\t- xQuery file path: " + xqueryFilePath);



        Collection col = null;

        try {

            // get the collection
            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);

            // get an instance of xquery service
            XQueryService xqueryService = (XQueryService) col.getService("XQueryService", "1.0");
            xqueryService.setProperty("indent", "yes");

            // read xquery
            System.out.println("[INFO] Invoking XQuery service for: " + xqueryFilePath);

            xqueryExpression = String.format(this.readFile(xqueryFilePath));
            System.out.println(xqueryExpression);

            // compile and execute the expression
            CompiledExpression compiledXquery = xqueryService.compile(xqueryExpression);
            ResourceSet result = xqueryService.execute(compiledXquery);

            // handle the results
            System.out.println("[INFO] Handling the results... ");

            ResourceIterator i = result.getIterator();
            Resource res = null;

            JAXBContext jaxbContext = JAXBContext.newInstance(Korisnici.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            while (i.hasMoreResources()) {

                try {
                    res = i.nextResource();
                    XMLResource xmlResource = (XMLResource) res;
                    PotvrdE p;
                    p = (PotvrdE) unmarshaller.unmarshal(xmlResource.getContentAsDOM());
                    return p;
                } finally {

                    // don't forget to cleanup resources
                    try {
                        ((EXistResource) res).freeResources();
                    } catch (XMLDBException xe) {
                        xe.printStackTrace();
                    }
                }

            }

            return null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            // don't forget to cleanup
            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
        // promeni
        return null;
    }

    public void saveDigitalniXml(DigitalCertificate saglasnost) throws Exception {
        String collectionId = DIGITALNI_COLLECTION_NAME;
        Collection col = null;
        XMLResource res = null;
        OutputStream os = new ByteArrayOutputStream();

        try {
            extractAndSaveMetadata();
            readDigitalniMetadata();

            System.out.println("[INFO] Retrieving the collection: " + collectionId);
            col = XmlDbConnectionUtils.getOrCreateCollection(collectionId);


            res = (XMLResource) col.createResource(saglasnost.getCertificateData().getCertificateId(), XMLResource.RESOURCE_TYPE);
            JAXBContext jaxbContext = JAXBContext.newInstance(DigitalCertificate.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            SchemaFactory sfZahtevi = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schemaZahtevi = sfZahtevi.newSchema(new File("../xml-documents/seme/digitalni_sertifikat.xsd"));

            jaxbMarshaller.setSchema(schemaZahtevi);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


            jaxbMarshaller.marshal(saglasnost, os);

            res.setContent(os);

            col.storeResource(res);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //don't forget to cleanup
            if (res != null) {
                try {
                    ((EXistResource) res).freeResources();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }

            if (col != null) {
                try {
                    col.close();
                } catch (XMLDBException xe) {
                    xe.printStackTrace();
                }
            }
        }
    }


    private void extractAndSaveMetadata() throws Exception {
        MetadataExtractor extractor = new MetadataExtractor();
        String xml = Files.readString(Path.of("src/main/resources/xmlFiles/xhtml/digitalni_sertifikat.xml"));
        InputStream in = new ByteArrayInputStream(xml.getBytes());

        OutputStream out = new FileOutputStream("src/main/resources/xmlFiles/rdf/metadata.rdf");

        extractor.extractMetadata(in, out);

        RdfDbConnectionUtils.writeMetadataToDatabase(DIGITALNI_NAMED_GRAPH_URI);
    }

    private void readDigitalniMetadata() throws Exception {
        RdfDbConnectionUtils.loadMetadataFromDatabase(DIGITALNI_NAMED_GRAPH_URI);
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);

    }

}
