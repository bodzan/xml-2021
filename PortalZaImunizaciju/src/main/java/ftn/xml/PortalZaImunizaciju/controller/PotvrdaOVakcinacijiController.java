package ftn.xml.PortalZaImunizaciju.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ftn.xml.PortalZaImunizaciju.service.PotvrdaOVakcinacijiService;

@RestController
@RequestMapping(value = "/api/potvrdaOVakcinaciji")
public class PotvrdaOVakcinacijiController {
	
	@Autowired
	PotvrdaOVakcinacijiService potvrdaService;
	
	@RequestMapping(value = "/read", method = RequestMethod.GET, produces = "application/xml")
    public ResponseEntity<Object> readXmlFile(@RequestParam String documentId) {
        try {
            Object potvrda = potvrdaService.readPotvrda(documentId);

            return new ResponseEntity<>(potvrda, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
	
	@RequestMapping(value = "/write", method = RequestMethod.GET)
    public ResponseEntity<String> writeXmlFile(@RequestParam String fileName) {
        try {
        	potvrdaService.writePotvrda(fileName);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
	
    // OLD method
	// @RequestMapping(value = "/create/{documentId}", method = RequestMethod.POST, consumes = "application/xml")
    // public ResponseEntity<String> writePotvrdaOVakcinacijiXML(@PathVariable String documentId, @RequestBody String xml) {
    //     try {
    //     	potvrdaService.writePotvrdaXml(documentId, xml);

    //         return new ResponseEntity<>(HttpStatus.OK);
    //     } catch (Exception e) {
    //         return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    //     }
    // }

    @PostMapping(value = "/create")
    public ResponseEntity<String> writeDigitalniXml(@RequestBody String xml) {
        try {


            potvrdaService.writePotvrdaXml(xml);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }

    }

}
