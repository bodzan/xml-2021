package ftn.xml.PortalZaImunizaciju.controller;

import ftn.xml.PortalZaImunizaciju.service.InteresovanjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/interesovanje")
public class InteresovanjeController {

    @Autowired
    private InteresovanjeService interesovanjeService;

    @PostMapping(value = "/createInteresovanje")
    public ResponseEntity<String> writeInteresovanjeXml(@RequestBody String xml) {
        try {

            interesovanjeService.saveInteresovanjeXml(xml);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }
    }


    @GetMapping(value = "/readInteresovanje/{id}", produces = "application/xml")
    public ResponseEntity<Object> findInteresovanje(@PathVariable String id) {

        try {
            Object saglasnost = interesovanjeService.findSaglasnostById(id);
            return new ResponseEntity<>(saglasnost, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
