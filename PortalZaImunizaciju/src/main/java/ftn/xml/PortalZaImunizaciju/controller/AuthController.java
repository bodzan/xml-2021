package ftn.xml.PortalZaImunizaciju.controller;

import ftn.xml.PortalZaImunizaciju.dto.rs.ac.un.ftn.korisnik.Login;
import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.korisnik.Korisnik;
import ftn.xml.PortalZaImunizaciju.security.TokenUtills;
import ftn.xml.PortalZaImunizaciju.service.KorisnikService;
import org.jline.utils.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    @Autowired
    private KorisnikService service;

    @Autowired
    TokenUtills tokenUtils;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @PostMapping(value = "/register", consumes = "application/xml", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> register(@RequestBody Korisnik korisnik) {
        try {
            boolean dodat = this.service.saveUser(korisnik);
            if (!dodat) {
                return new ResponseEntity<>("User already exists!", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<>("ERROR", HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> login(@RequestBody Login login) {
        try {

            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login.getUsername(),
                    login.getPassword());

            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            HttpHeaders headers = new HttpHeaders();

            // Reload user details so we can generate token
            Korisnik korisnik = (Korisnik) authentication.getPrincipal();
            String authToken = tokenUtils.generateToken(korisnik);
//            System.out.println(authToken);
            headers.add("Access-Control-Expose-Headers", "token");
            headers.add("token", authToken);
//            Korisnik k = this.service.login(login.getPassword());

            return new ResponseEntity<>(null, headers, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }


}

