package ftn.xml.PortalZaImunizaciju.controller;

import ftn.xml.PortalZaImunizaciju.model.email.EmailTemplate;
import ftn.xml.PortalZaImunizaciju.model.rs.ac.uns.ftn.saglasnost.SaglasnostZaSprovodjenjeImunizacije;
import ftn.xml.PortalZaImunizaciju.repository.SaglasnostRepository;
import ftn.xml.PortalZaImunizaciju.service.SaglasnostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@RestController
@RequestMapping(value = "/api/saglasnost")
public class SaglasnostController {

    @Autowired
    public SaglasnostService saglasnostService;


    @GetMapping(value = "/read/{id}", produces = "application/xml")
    public ResponseEntity<Object> findSaglasnost(@PathVariable String id) {

        try {
            Object saglasnost = saglasnostService.findSaglasnostById(id);
            return new ResponseEntity<>(saglasnost, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/create")
    public ResponseEntity<String> writeSaglasnostXml(@RequestBody String xml) {
        try {

            saglasnostService.saveSaglasnostXML(xml);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        }

    }

    @GetMapping(value = "email")
    public ResponseEntity<String> sendEmailWithAttachment() {
        sendEmail();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void sendEmail() {
//        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        String korisnik = "ggwp012@gmail.com";
        EmailTemplate  emailTemplate = new EmailTemplate();
        emailTemplate.setAttachmentPath("D:\\Users\\HpZbook15\\Desktop\\xml-2021\\PortalZaImunizaciju\\src\\main\\resources\\xmlFiles\\pdf\\obrazac-saglasnosti-za-imunizaciju.pdf");
        emailTemplate.setBody("HTML kreiranog zahteva.");
        emailTemplate.setSentFrom("Servis organa vlasti");
        emailTemplate.setSubject("Uspesno kreiran zahtev");
        emailTemplate.setSendTo(korisnik);


        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<EmailTemplate> entity = new HttpEntity<>(emailTemplate);


        restTemplate.postForEntity("http://localhost:6969/email/send/attachemail", entity, String.class);
    }
}
