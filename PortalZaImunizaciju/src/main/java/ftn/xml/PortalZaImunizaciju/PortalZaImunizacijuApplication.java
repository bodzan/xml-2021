package ftn.xml.PortalZaImunizaciju;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalZaImunizacijuApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortalZaImunizacijuApplication.class, args);
	}

}
