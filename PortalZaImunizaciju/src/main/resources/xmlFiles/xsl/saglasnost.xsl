<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sgl="http://www.ftn.uns.ac.rs/saglasnost"
                xmlns:pred="http://www.ftn.uns.ac.rs/rdf/saglasnost/predicate/"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0">
    <xsl:template match="/">
        <sgl:saglasnost_za_sprovodjenje_imunizacije>
            <sgl:naslov>
                <xsl:value-of select="sgl:saglasnost/sgl:naslov"></xsl:value-of>
            </sgl:naslov>
            <sgl:pacijent about="http://www.ftn.uns.ac.rs/rdf/saglasnost/pacijent">
                <sgl:drzavljanstvo>
                    <sgl:strano>
                        <sgl:naziv_drzavljanstva>naziv_drzavljanstva0</sgl:naziv_drzavljanstva>
                        <sgl:broj_pasosa>0</sgl:broj_pasosa>
                    </sgl:strano>
                </sgl:drzavljanstvo>
                <sgl:prezime property="pred:prezime">Petrovic</sgl:prezime>
                <sgl:ime property="pred:ime">Petar</sgl:ime>
                <sgl:ime_roditelja property="pred:ime_r">Dragan</sgl:ime_roditelja>
                <sgl:pol>pol0</sgl:pol>
                <sgl:datum_rodjenja>2006-05-04</sgl:datum_rodjenja>
                <sgl:mesto_rodjenja>mesto_rodjenja0</sgl:mesto_rodjenja>
                <sgl:adresa>adresa0</sgl:adresa>
                <sgl:fiksni_telefon>fiksni_telefon0</sgl:fiksni_telefon>
                <sgl:email property="pred:email">pera@mail.com</sgl:email>
                <sgl:mobilni-telefon>mobilni-telefon0</sgl:mobilni-telefon>
                <sgl:korisnik_ustanove_socijalne_zastite korisnik="false">
                </sgl:korisnik_ustanove_socijalne_zastite>
            </sgl:pacijent>
            <sgl:saglasnost saglasan="true">
                <sgl:naziv_leka>naziv_leka0</sgl:naziv_leka>
            </sgl:saglasnost>
            <sgl:evidencija_o_vakcinaciji>
                <sgl:zdravstvena_ustanova>zdravstvena_ustanova0</sgl:zdravstvena_ustanova>
                <sgl:vakcinacijski_punkt>vakcinacijski_punkt0</sgl:vakcinacijski_punkt>
                <sgl:podaci_lekara>
                    <sgl:ime>ime1</sgl:ime>
                    <sgl:prezime>prezime1</sgl:prezime>
                    <sgl:faksimil>faksimil0</sgl:faksimil>
                    <sgl:broj_telefona>broj_telefona0</sgl:broj_telefona>
                </sgl:podaci_lekara>
                <sgl:prva_doza>
                    <sgl:naziv_vakcine>naziv_vakcine0</sgl:naziv_vakcine>
                    <sgl:datum_davanja>2006-05-04</sgl:datum_davanja>
                    <sgl:nacin_davanja>IM</sgl:nacin_davanja>
                    <sgl:ekstremitet>ekstremitet0</sgl:ekstremitet>
                    <sgl:serija_vakcine>0</sgl:serija_vakcine>
                    <sgl:proizvodjac>proizvodjac0</sgl:proizvodjac>
                    <sgl:nezeljena_reakcija>nezeljena_reakcija0</sgl:nezeljena_reakcija>
                </sgl:prva_doza>
                <sgl:druga_doza>
                    <sgl:naziv_vakcine>naziv_vakcine1</sgl:naziv_vakcine>
                    <sgl:datum_davanja>2006-05-04</sgl:datum_davanja>
                    <sgl:nacin_davanja>IM</sgl:nacin_davanja>
                    <sgl:ekstremitet>ekstremitet1</sgl:ekstremitet>
                    <sgl:serija_vakcine>0</sgl:serija_vakcine>
                    <sgl:proizvodjac>proizvodjac1</sgl:proizvodjac>
                    <sgl:nezeljena_reakcija>nezeljena_reakcija1</sgl:nezeljena_reakcija>
                </sgl:druga_doza>
                <sgl:kontraindikacije/>
            </sgl:evidencija_o_vakcinaciji>
        </sgl:saglasnost_za_sprovodjenje_imunizacije>
    </xsl:template>
</xsl:stylesheet>